const Conge = require('../model/conge');
const User = require("../model/user");

const getCongeCeMois = async (req, res) => {
    try {
        const start = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        const end = new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0);

        const conges = await Conge.find({ startDate: { $gte: start, $lte: end } });
        res.json(conges);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const getStatusDistribution = async (req, res) => {
    try {
      const approved = await Conge.countDocuments({ status: 'Approved' });
      const rejected = await Conge.countDocuments({ status: 'Rejected' });
      const pending = await Conge.countDocuments({ status: 'Pending' });
      
      res.json({ approved, rejected, pending });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  };

const getSolde = async (req, res) => {
    try {
        const users = await User.find({}, 'solde email');
        res.json(users);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const roleDistribution = async (req, res) => {
    try {
        const rh = await User.countDocuments({ role: 'rh' });
        const admin = await User.countDocuments({ role: 'admin' });
        const employee = await User.countDocuments({ role: 'employee' });

        res.json({ rh, admin, employee });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}
module.exports = { getCongeCeMois,getStatusDistribution,getSolde,roleDistribution };