const User = require("../model/user");
const bcrypt = require("bcryptjs");


// Fonction pour supprimer un utilisateur par son ID
async function deleteUser(req, res) {
    try {
        const userId = req.params.id;

        
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        
        await user.remove();

        res.status(200).json({ message: "User deleted successfully" });
    } catch (error) {
        console.error("Error deleting user:", error);
        res.status(500).json({ message: "Internal server error" });
    }
}

// Fonction pour mettre à jour un utilisateur par son ID
async function updateUser(req, res) {
    try {
        const userId = req.params.id; 
        const userData = req.body; 

        // Vérifier si l'utilisateur existe
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        user.set(userData);
        const updatedUser = await user.save();

        res.status(200).json(updatedUser);
    } catch (error) {
        console.error("Error updating user:", error);
        res.status(500).json({ message: "Internal server error" });
    }
}


// Fonction pour récupérer un utilisateur par son ID
async function getUser(req, res) {
    try {
        const userId = req.params.id;
        const user = await User.findById(userId);

        
        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        
        res.status(200).json(user);
    } catch (error) {

        console.error("Error fetching user:", error);
        res.status(500).json({ message: "Internal server error" });
    }
}

// Fonction pour recupere  les utilisateurs lkoool
async function getUsers(req, res) {
    try {
        const users = await User.find();

        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ message: "Error fetching users" });
    }
}

//creation d un user
const createUser = async (req, res) => {
    const userData = req.body;
    try {

        const newUser = new User(req.body);
        
        if (userData.email == "" || userData.password == "") {
            return res.status(400).json({ message: "email or password is empty" })
        }

    
        const existUser = await User.findOne({ email: userData.email });
        if (existUser) {
            return res.status(400).json({ message: "email already exist" })
        }

        
        const user = await newUser.save();
        res.status(200).json(user);
    } catch (err) {
        res.status(500).json(err);
    }

}
const getEmployee = async (req, res) => {
    try {
        
        const users = await User.find({ role: "employee" });
        if(!users){
            return res.status(404).json({ message: "User not found" });
        }
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json({ message: "Error fetching users" });
    }
}
const getProfileImage = async (req, res) => {
    const userId = req.user.id;
    try {
        const user = await User.findById(userId);
        res.redirect(`/public/images/${user.profileImage}`);
        //res.status(200).json(user.profileImage);
    } catch (error) {
        console.error('Error getting user:', error);
        res.status(500).json({ error: 'Error getting user' });
    }
}

const addImageProfile = async (req, res) => {
    const userId = req.user.id;
    if (!req.file) {
        return res.status(400).json({ error: 'No file uploaded' });
    }
    const filename = req.file.filename;
    try {
        const updatedUser = await User.findByIdAndUpdate(userId, { profileImage: filename }, { new: true });
        res.status(201).json(updatedUser);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Error updating resource' });
    }
}

module.exports = { createUser,addImageProfile ,getProfileImage,getUsers, getUser, updateUser, deleteUser,getEmployee };
