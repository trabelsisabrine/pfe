const Conge = require("../model/conge");
const User = require('../model/user')
const { intervalToDuration } = require("date-fns");
const UserInfo = require("../model/user-info");

const createConge = async (req, res) => {
    const { startDate, endDate, reason, nbjours } = req.body;
    const employeeId = req.user.id
    try {
        const newConge = new Conge({
            employee: employeeId,
            startDate,
            endDate,
            reason,
        });

        // Sauvegarde le congé dans la db
        const congé = await newConge.save();

        res.status(201).json(congé);
    } catch (error) {
        console.error("Error creating congé:", error);
        if (error.name === 'ValidationError') {
            return res.status(400).json({ message: error.message });
        }
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};


const getConges = async (req, res) => {
    try {
        
        const congés = await Conge.find().populate({
            path: 'employee',
            select: 'email'  
        });



        res.status(200).json(congés);
    } catch (error) {
        console.error("Erreur lors de la récupération des congés:", error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

const getMyConge = async (req, res) => {
    const employeeId = req.user.id;
    try {
        const congés = await Conge.find({ employee: employeeId });
        res.status(200).json(congés);
    } catch (error) {
        console.error("Error fetching my leaves:", error);
        res.status(500).json({ message: "Internal server error" });
    }
};

// Fonction pour récupérer un congé par son ID
const getCongeById = async (req, res) => {
    try {
        const congéId = req.params.id;

        const congé = await Conge.findById(congéId);

       
        if (!congé) {
            return res.status(404).json({ message: "Congé not found" });
        }

        res.status(200).json(congé);
    } catch (error) {
        console.error("Error fetching congé:", error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

// Fonction pour mettre à jour un congé par son ID
const updateConge = async (req, res) => {
    try {
        const congéId = req.params.id;
        const updates = req.body;

        
        const updatedConge = await Conge.findByIdAndUpdate(congéId, updates, { new: true });

        
        if (!updatedConge) {
            return res.status(404).json({ message: "Congé n'existe pas" });
        }

        res.status(200).json(updatedConge);
    } catch (error) {
        console.error("Error updating congé:", error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};



const accpetConge = async (req, res) => {
    try {
        const congeId = req.params.id;

        // Find the leave request by ID and populate user info
        const conge = await Conge.findById(congeId).populate('employee');

        if (!conge) {
            return res.status(404).json({ error: "Leave request not found" });
        }

        const { startDate, endDate, employee } = conge;
        const interval = intervalToDuration({ start: startDate, end: endDate });


        if (employee.solde < interval.days) {
            return res.status(400).json({ error: "Solde de congés insuffisant" });
        }

        
        const status = (interval.months > 0 || interval.days > 25) ? "Rejected" : "Approved";

        const updatedConge = await Conge.findByIdAndUpdate(congeId, { status }, { new: true });

        const newSold = employee.solde - interval.days;
        const updatedUser = await User.findByIdAndUpdate(employee._id, { solde: newSold }, { new: true });

        res.status(200).json({ conge: updatedConge, user: updatedUser });
    } catch (err) {
        console.error("Error accepting leave:", err);
        res.status(500).json({ error: "Internal server error" });
    }
};


const refuserConge = async (req, res) => {

    const congeId = req.params.id;
    try {
        const refuserStatus = await Conge.findByIdAndUpdate(congeId, { status: "Rejected" }, { new: true });
        res.status(200).json(refuserStatus)
    } catch (err) {
        res.status(500).json({ message: "Internal server error" });

    }
}
// Fonction pour supprimer un congé par son ID
const deleteConge = async (req, res) => {
    try {
        const congéId = req.params.id;

        const deletedConge = await Conge.findByIdAndDelete(congéId);

        if (!deletedConge) {
            return res.status(404).json({ message: "Congé not found" });
        }

        res.status(200).json({ message: "Congé deleted successfully" });
    } catch (error) {
        console.error("Error deleting congé:", error);
        res.status(500).json({ message: "Internal server error" });
    }
};

const getCongeByEmployeeId = async (req, res) => {
    const employeeId = req.params.id;
    try {
        const conges = await Conge.find({ employee: employeeId });
        res.status(200).json(conges);
    } catch (error) {
        console.error("Error fetching leaves:", error);
        res.status(500).json({ message: "Internal server error" });
    }
}

module.exports = { createConge, getCongeByEmployeeId, accpetConge, refuserConge, getConges, getCongeById, updateConge, deleteConge, getMyConge };
