const UserInfo = require("../model/user-info");

const createUserInfo = async (req,res)=>{
    const userId =req.user.id
    const userInfo = req.body
    try {
        const existingUser = await UserInfo.findOne({employee:userId})
        if(existingUser){
            return res.status(400).json({"error":"c'est deja u information  "})
        }
        const newUserInfo= new UserInfo({
            employee:userId,
            ...userInfo
        })

        const info = await newUserInfo.save()
        return res.status(201).json(info);

    }catch(err){
        res.status(500).json({ message: "Erreur interne du serveur" });

    }
}

const getUserInfo = async(req,res)=>{
    const userId = req.user.id
    try{
        const userInfo = await UserInfo.findOne({employee:userId})
        if(!userInfo){
            return res.status(404).json({"error":"pas encore d'information"})
        }
        return res.status(200).json(userInfo)
    }catch(err){
        res.status(500).json({ message: "Erreur interne du serveur" });

    }

}

const updateUserInfo = async(req,res)=>{
    const userId = req.user.id
    const userInfo = req.body
    try{
        const existingUser = await UserInfo.findOne({employee:userId})
        if(!existingUser){
            return res.status(404).json({"error":"y a pas dinformation"})
        }
        const info = await UserInfo.findOneAndUpdate({employee:userId},userInfo,{new:true})
        return res.status(200).json(info)
    }catch(err){
        res.status(500).json({ message: "Erreur interne du serveur" });

    }
}
module.exports = {createUserInfo,getUserInfo,updateUserInfo}