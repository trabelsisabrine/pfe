const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../model/user");

// Fonction pour la connexion (Sign In)
const signIn = async (req, res) => {
    try {
        const { email, password } = req.body;

        // Vérifier si l'utilisateur existe
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(401).json({ message: "Email invalide" });
        }

        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) {
            return res.status(401).json({ message: "Mot de passe invalide" });
        }
        // Vérifier si le mot de passe est correct

        // Générer le token d'authentification
        const accessToken = jwt.sign({ user:{ id:user._id ,role:user.role} }, 
                                       process.env.ACCESS_TOKEN_SECRET,
                                    {  expiresIn: "1h" });

        const userInfo = { ...user._doc, password: undefined }
        
        res.status(200).json({ accessToken, userInfo });
    } catch (error) {
        console.error("Error signing in:", error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

// Fonction pour l'inscription (Sign Up)
const signUp = async (req, res) => {
    try {
        const { email, password, role } = req.body;

        // Vérifier si l'utilisateur existe déjà
        const existingUser = await User.findOne({ email });
        if (existingUser) {
            return res.status(400).json({ message: "Email already exists" });
        }

        // Hasher le mot de passe
        const hashedPassword = await bcrypt.hash(password, 10);

        // Créer un nouvel utilisateur
        const newUser = new User({
            email,
            password: hashedPassword,
            role 
        });

        // Sauvegarder le nouvel utilisateur dans la base de données
        const user = await newUser.save();

        // Répondre avec le token et les détails de l'utilisateur
        res.status(201).json({ message: "utilisateur etait creé" });
    } catch (error) {
        console.error("Error signing up:", error);
        res.status(500).json({ message: "Erreur interne du serveur" });
    }
};

module.exports = { signIn, signUp };
