const express = require("express");
const router = express.Router();

const { getCongeCeMois, getStatusDistribution ,getSolde,roleDistribution} = require("../controller/kpi");

router.get('/conges/current-month', getCongeCeMois);

router.get('/conges/status-distribution', getStatusDistribution)

router.get('/users/solde', getSolde);

router.get('/users/roles-distribution', roleDistribution);

module.exports = router;
