const router = require("express").Router();
const { createUser,addImageProfile, getProfileImage, getUsers, getUser, updateUser, deleteUser,getEmployee } = require("../controller/user");
const multerConfig = require('../config/multer');
const multer = require('multer');


router.get('/get-profile-img', getProfileImage);

router.post('/upload-img-profile', multer(multerConfig).single('file'), (req, res) => {
    try {
        if (res.status(200)) {
            addImageProfile(req, res)
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
})
router.get("/all", getUsers);
router.get("/employee",getEmployee);
router.post("/", createUser);
router.get("/:id", getUser);
router.put("/:id", updateUser);
router.delete("/:id", deleteUser);



module.exports = router;
