const express = require("express");
const router = express.Router();
const { createConge,accpetConge,refuserConge, getConges, getCongeById, updateConge, deleteConge,getMyConge,getCongeByEmployeeId } = require("../controller/conge");

router.post("/create", createConge);
router.get('/mon-conge',getMyConge);
router.get("/employee/:id", getCongeByEmployeeId)
router.get("/all", getConges);
router.get("/:id", getCongeById);
router.put("/update/:id", updateConge);
router.delete("/delete/:id", deleteConge);

router.put("/accepter/:id",accpetConge)
router.put("/refuser/:id",refuserConge)

module.exports = router;
