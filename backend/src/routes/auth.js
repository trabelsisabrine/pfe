const router = require("express").Router();

const { signIn, signUp } = require("../controller/auth");

router.post('/login',signIn)
router.post("/register", signUp);


module.exports = router;
