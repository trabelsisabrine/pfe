const router = require("express").Router();

const {createUserInfo,getUserInfo,updateUserInfo} = require('../controller/user-info');

router.post('/',createUserInfo)
router.get('/',getUserInfo)
router.put('/',updateUserInfo)
module.exports = router;
