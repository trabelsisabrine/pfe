const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv");
const mongoose = require("mongoose");

const authenticateToken = require("./middleware/authenticationToken")

const userRoute = require("./routes/user");
const authRoute = require ("./routes/auth")
const congeRoutes = require("./routes/conge"); 
const userInfo = require("./routes/user-info");
const kpi = require("./routes/kpi");
const port = process.env.PORT || 4000;

const app = express();
dotenv.config();
app.use(cors());
app.use(express.json());

mongoose.set("strictQuery", false);
mongoose.connect(process.env.URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.error(err));


app.use("/api/auth",authRoute)

app.use("/api/user", authenticateToken,userRoute);
app.use("/api/conges",authenticateToken, congeRoutes);
app.use("/api/userinfo",authenticateToken,userInfo);
app.use('/api/kpi', authenticateToken, kpi);
app.use('/public/images', express.static('./public/images'));


app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});