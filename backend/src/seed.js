const mongoose = require("mongoose");
const User = require("./model/user");  
const dotenv = require("dotenv");
const bcrypt = require("bcryptjs");

dotenv.config(); 


mongoose.set("strictQuery", false);
mongoose.connect(process.env.URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.error(err));

async function seedUsers() {
    try {
        const hashedPassword = await bcrypt.hash('sabrine@admin.com', 10);  

        const userData = {
            email: "sabrine@admin.com",
            password: hashedPassword,
            profileImage:"...",
            role: "admin",
            solde: 25
        }


        const user = new User(userData);
        await user.save();

        console.log("User seeded successfully:", user);
    } catch (error) {
        console.error("Error seeding user:", error);
    } finally {
        mongoose.disconnect();
        console.log("MongoDB disconnected");
    }
}

seedUsers();