const mongoose = require("mongoose");


const congeSchema = new mongoose.Schema({
    employee: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }, 
    startDate: { type: Date, required: true },
    endDate: { type: Date, required: true },
    status: { type: String, enum: ['Pending', 'Approved', 'Rejected'], default: 'Pending' },
    reason: { type: String, required: true }

});

// Créer le modèle Conge à partir du schéma
const Conge = mongoose.model("Conge", congeSchema);


module.exports = Conge;