const mongoose = require("mongoose");
const UserInfoSchema = new mongoose.Schema(
    {
        name: {
            type: String
        },
        lastname: {
            type: String
        },
        age:{
            type:Number
        },
       
        employee: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },

    },
    { timestamps: true }
);
module.exports = mongoose.model("UserInfo", UserInfoSchema);