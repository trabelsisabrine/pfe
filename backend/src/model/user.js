const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    profileImage: {
      type: String
    },
    role: {
      type: String,
      enum: ['employee', 'admin', 'rh'],
      default: 'employee'
    },
    solde: {
      type: Number,
      default: 25
    }

  },
  { timestamps: true }
);
module.exports = mongoose.model("User", UserSchema);