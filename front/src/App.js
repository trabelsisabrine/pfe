import React from 'react';
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Navigate, Outlet } from 'react-router-dom';
import ProtectedRoute from "./components/ProtectedRoute";

import Login from "./pages/auth/login";
import Admin from "./pages/admin";
import Employee from "./pages/employee";
import Rh from "./pages/rh";
import Users from "./pages/admin/components/user";
import Conge from "./pages/employee/components/conge";  
import AdminProfile from "./pages/admin/components/profile";
import GerantProfile from "./pages/employee/components/profile";
import RhProfile from "./pages/rh/components/profile";
import Register from './pages/auth/register';
import GererConge from './pages/rh/components/gerer-conge'
import TraitementDemande from './pages/rh/components/Traitement-Demande'
import Dashboard from './pages/admin/components/dashboard';
import AdminGererConge from './pages/admin/components/gerer-conge-admin';
import AdminTraitementDemande from './pages/admin/components/Traitement-Demande-admin';
function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Outlet />,
      children: [
        { path: "/", element: <Navigate to="/login" replace /> },
        
        { path: "register", element: <Register /> },
        { path: "login", element: <Login /> },
        {
          path: "admin",
          element: <ProtectedRoute roleRequired="admin" />,
          children: [
            {
              path: "",
              element: <Admin />,
              children: [
                { path: "dashboard", element: <Dashboard />},
                { path: "users", element: <Users /> },
                { path: "profile", element: <AdminProfile />},
                { path: "traitement-demande",element : <AdminTraitementDemande />},
                { path:"gerer-conge",element: <AdminGererConge />},

              ]
            }
          ],
        },
        {
          path: "employee",
          element: <ProtectedRoute roleRequired="employee" />,
          children: [
            {
              path: "",
              element: <Employee />,
              children: [
                { path: "conge", element: <Conge /> },
                { path: "profile", element: <GerantProfile />},
                
                { path:"demande", element: <h1>Demande</h1>},
              ]
            }
          ],
        },
        {
          path: "rh",
          element: <ProtectedRoute roleRequired="rh" />,
          children: [
            {
              path: "",
              element: <Rh />,
              children: [
              
                {path:"traitement-demande", element: <TraitementDemande />},
                {path:"gerer-conge",element: <GererConge />},
                {path:"profile", element: <RhProfile /> }
              ]
            }
          ],
        }
      ],
    },
    { path: "*", element: <Navigate to="/login" replace /> }
  ]);

  return (
    <div>
      <RouterProvider router={router} />
    </div>
  );
}

export default App;
