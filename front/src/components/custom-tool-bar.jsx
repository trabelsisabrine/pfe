import { GridToolbarContainer, GridToolbarColumnsButton, GridToolbarFilterButton, GridToolbarExport } from '@mui/x-data-grid';
import { Button } from '@mui/material';

const CustomToolbar = ({ onAdd, onDelete, onModify, showAddButton = true, showModifyButton = true, showDeleteButton = true }) => {
    return (
        <GridToolbarContainer sx={{ pb: 2 }}>
            {showAddButton && (
                <Button onClick={onAdd} color="secondary" >
                    ajouter
                </Button>
            )}
            {showModifyButton && (
                <Button onClick={onModify} color="secondary">
                    Modifier
                </Button>
            )}
            {showDeleteButton && (
                <Button onClick={onDelete} color="secondary">
                    supprimer
                </Button>
            )}
          
            <div>
                <GridToolbarColumnsButton color="secondary" />
                <GridToolbarFilterButton color="secondary" />
                <GridToolbarExport color="secondary" />
            </div>
        </GridToolbarContainer>
    );
};

export default CustomToolbar;
