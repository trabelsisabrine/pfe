import React, { useEffect, useState } from 'react';
import { Box, TextField, Button, Typography, Container, Snackbar } from '@mui/material';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
    email: Yup.string().email('Invalid email format').required('Required'),
    password: Yup.string().required('Required').min(8, 'Password should be at least 8 characters long'),
});

const RhProfile = () => {
    const token = useSelector(state => state.auth.user.accessToken);
    const userId = useSelector(state => state.auth.user.userInfo._id);
    const [user, setUser] = useState({ email: '', password: '' });
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState("");

    useEffect(() => {
        fetchUser();
    }, [userId, token]);

    const fetchUser = async () => {
        const url = `http://localhost:4000/api/user/${userId}`;
        try {
            const response = await axios.get(url, {
                headers: { Authorization: `Bearer ${token}` },
            });
            setUser({ email: response.data.email, password: response.data.password }); 
        } catch (error) {
            console.error('Failed to fetch user:', error);
        }
    };

    const handleSave = async (values) => {
        const url = `http://localhost:4000/api/user/${userId}`;
        try {
            const response = await axios.put(url, values, {
                headers: { Authorization: `Bearer ${token}` },
            });
            setSnackbarMessage('Mise à jour du profil réussie!!');
            setOpenSnackbar(true);
        } catch (error) {
            console.error('Failed to update profile:', error);
            setSnackbarMessage('Error updating profile!');
            setOpenSnackbar(true);
        }
    };

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    return (
        <Container maxWidth="xs">
            <Box sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Typography component="h1" variant="h5">
                    Edit Profile
                </Typography>
                <Formik
                    initialValues={user}
                    enableReinitialize // Important: reinitialize form when initialValues change
                    validationSchema={validationSchema}
                    onSubmit={handleSave}
                >
                    {({ errors, touched }) => (
                        <Form noValidate sx={{ mt: 1 }}>
                            <Field
                                as={TextField}
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                margin="normal"
                                error={errors.email && touched.email}
                                helperText={errors.email && touched.email ? errors.email : ""}
                            />
                            <Field
                                as={TextField}
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="new-password"
                                margin="normal"
                                error={errors.password && touched.password}
                                helperText={errors.password && touched.password ? errors.password : ""}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb :2 }}
                            >
                                Save Changes
                            </Button>
                        </Form>
                    )}
                </Formik>
                <Snackbar
                    open={openSnackbar}
                    autoHideDuration={6000}
                    onClose={handleCloseSnackbar}
                    message={snackbarMessage}
                />
            </Box>
        </Container>
    );
};

export default RhProfile;
