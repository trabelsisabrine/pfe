import React, { useState, useEffect } from 'react';
import {
    Box, useTheme, Dialog, DialogTitle, DialogContent, DialogActions,
    Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography
} from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { format } from 'date-fns';

import { tokens } from "../../../../theme";
import Header from "../../../../components/Header";
import CustomToolbar from '../../../../components/custom-tool-bar';

const GererConge = () => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [employeeData, setEmployeeData] = useState([]);
    const [employeeConges, setEmployeeConges] = useState([]); 
    const [open, setOpen] = useState(false);
    const token = useSelector(state => state.auth.user.accessToken);

    useEffect(() => {
        fetchEmployees();
    }, [token]);

    const fetchEmployees = async () => {
        try {
            const response = await axios.get('http://localhost:4000/api/user/employee', {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            setEmployeeData(response.data);
        } catch (error) {
            console.error('Failed to fetch employees:', error);
        }
    };
    
    const fetchCongesByEmployee = async (id) => {
        try {
            const response = await axios.get(`http://localhost:4000/api/conges/employee/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            setEmployeeConges(response.data);
            setOpen(true);
        } catch (error) {
            console.error('Failed to fetch conges for employee:', error);
        }
    };

    const handleRowClick = (params) => {
        fetchCongesByEmployee(params.row._id);
    };

    const columns = [
        { field: 'email', headerName: 'Email', width: 200 },
        { field: 'solde', headerName: 'Solde', type: 'number', width: 100 },
    ];

    return (
        <Box m="20px">
            <Header title="Gérer les Congés" subtitle="Liste des Congés" />
            <Box
                m="40px 0 0 0"
                height="75vh"
                sx={{
                    "& .MuiDataGrid-root": { border: "none" },
                    "& .MuiDataGrid-cell": { borderBottom: "none" },
                    "& .MuiDataGrid-columnHeaders": { backgroundColor: colors.blueAccent[700], borderBottom: "none" },
                    "& .MuiDataGrid-virtualScroller": { backgroundColor: colors.primary[400] },
                    "& .MuiDataGrid-footerContainer": { borderTop: "none", backgroundColor: colors.blueAccent[700] },
                    "& .MuiCheckbox-root": { color: `${colors.greenAccent[200]} !important` },
                }}
            >
                <DataGrid
                    rows={employeeData}
                    columns={columns}
                    getRowId={(row) => row._id} 
                    onRowClick={handleRowClick}
                    components={{ Toolbar: CustomToolbar }}
                    componentsProps={{
                        toolbar: {
                            showModifyButton: false,
                            showAddButton: false,
                            showDeleteButton: false,
                        },
                    }}
                />
            </Box>
            {/* Dialog to display employee's conges details */}
            <Dialog open={open} onClose={() => setOpen(false)} fullWidth maxWidth="md">
                <DialogTitle>Détails des Congés</DialogTitle>
                <DialogContent>
                    <TableContainer component={Paper}>
                        <Table aria-label="conge details table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Start Date</TableCell>
                                    <TableCell>End Date</TableCell>
                                    <TableCell>Status</TableCell>
                                    <TableCell>Reason</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {employeeConges.map((conge) => (
                                    <TableRow key={conge._id}>
                                        <TableCell>{format(new Date(conge.startDate), 'dd/MM/yyyy')}</TableCell>
                                        <TableCell>{format(new Date(conge.endDate), 'dd/MM/yyyy')}</TableCell>
                                        <TableCell>{conge.status}</TableCell>
                                        <TableCell>{conge.reason}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpen(false)}>Fermer</Button>
                </DialogActions>
            </Dialog>
        </Box>
    );
};

export default GererConge;
