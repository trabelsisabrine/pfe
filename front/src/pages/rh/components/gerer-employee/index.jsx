import React, { useState, useEffect } from 'react';
import {
    Box, useTheme, Dialog, DialogTitle, DialogContent, DialogActions,
    Button, TextField, Grid
} from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useSelector } from 'react-redux';

import { tokens } from "../../../../theme";
import Header from "../../../../components/Header";
import CustomToolbar from '../../../../components/custom-tool-bar';
import * as Yup from 'yup';
import Toast from 'react-hot-toast';
import { Formik, Form, Field } from 'formik';

const employeeValidationSchema = Yup.object({
    email: Yup.string().email('Invalid email').required('Email is required'),
    solde: Yup.number().min(0, 'Solde cannot be negative').required('Solde is required'),
});

const EmployeeManagement = () => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [employeeData, setEmployeeData] = useState([]);
    const [selectionModel, setSelectionModel] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);
    const [editEmployee, setEditEmployee] = useState({});
    const token = useSelector(state => state.auth.user.accessToken);

    useEffect(() => {
        fetchEmployees();
    }, [token]);

    const fetchEmployees = async () => {
        try {
            const response = await axios.get('http://localhost:4000/api/user/employee', {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            setEmployeeData(response.data);
        } catch (error) {
            console.error('Failed to fetch employees:', error);
        }
    };

    const handleAdd = () => {
        setEditEmployee({
            email: "", solde: ""
        });
        setOpenDialog(true);
    };

    const handleModify = () => {
        if (selectionModel.length === 1) {
            const employeeToEdit = employeeData.find(emp => emp._id === selectionModel[0]);
            setEditEmployee(employeeToEdit);
            setOpenDialog(true);
        } else {
            alert("Please select exactly one employee to edit.");
        }
    };

    const handleSaveChanges = async (values) => {
        const isNew = !editEmployee._id;
        const url = isNew ? 'http://localhost:4000/api/auth/register' : `http://localhost:4000/api/user/${editEmployee._id}`;
        try {
            const response = await axios({
                method: isNew ? 'post' : 'put',
                url: url,
                data: values,
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            Toast.success(isNew ? 'Employee registered successfully' : 'Employee updated successfully');
            const updatedEmployees = isNew ? [...employeeData, response.data] : employeeData.map(employee => employee._id === editEmployee._id ? response.data : employee);
            setEmployeeData(updatedEmployees);
            setOpenDialog(false);
        } catch (error) {
            console.error('Failed to save employee:', error);
        }
    };

    const handleDelete = async () => {
        const selectedIDs = Array.from(new Set(selectionModel));
        if (selectedIDs.length > 0) {
            try {
                await Promise.all(selectedIDs.map(id =>
                    axios.delete(`http://localhost:4000/api/user/${id}`, {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    })
                ));
                Toast.success('Employee(s) deleted successfully');
                setEmployeeData(employeeData.filter(employee => !selectedIDs.includes(employee._id)));
            } catch (error) {
                console.error('Failed to delete employee:', error);
            }
        } else {
            alert("Please select at least one employee to delete.");
        }
    };

    const handleClose = () => {
        setOpenDialog(false);
    };

    const columns = [
        { field: '_id', headerName: 'ID', width: 220 },
        { field: 'email', headerName: 'Email', width: 200 },
        { field: 'solde', headerName: 'Solde', type: 'number', width: 100 },
    ];

    return (
        <Box m="20px">
            <Header title="Employee Management" subtitle="List of Employees" />
            <Box
                m="40px 0 0 0"
                height="75vh"
                sx={{
                    "& .MuiDataGrid-root": { border: "none" },
                    "& .MuiDataGrid-cell": { borderBottom: "none" },
                    "& .MuiDataGrid-columnHeaders": { backgroundColor: colors.blueAccent[700], borderBottom: "none" },
                    "& .MuiDataGrid-virtualScroller": { backgroundColor: colors.primary[400] },
                    "& .MuiDataGrid-footerContainer": { borderTop: "none", backgroundColor: colors.blueAccent[700] },
                    "& .MuiCheckbox-root": { color: `${colors.greenAccent[200]} !important` },
                }}
            >
                <DataGrid
                    checkboxSelection
                    rows={employeeData}
                    columns={columns}
                    getRowId={(row) => row._id} 

                    components={{ Toolbar: CustomToolbar }}
                    onSelectionModelChange={(newSelectionModel) => {
                        setSelectionModel(newSelectionModel);
                    }}
                    componentsProps={{
                        toolbar: {
                            onAdd: handleAdd,
                            onDelete: handleDelete,
                            onModify: handleModify,
                            showModifyButton: true,
                            showAddButton: false,
                            showDeleteButton: true,
                        },
                    }}
                />
            </Box>
            <Dialog open={openDialog} onClose={handleClose}>
                <Formik
                    initialValues={{
                        email: editEmployee.email || '',
                        solde: editEmployee.solde || '',
                    }}
                    validationSchema={employeeValidationSchema}
                    onSubmit={(values, { setSubmitting }) => {
                        handleSaveChanges(values);
                        setSubmitting(false);
                    }}
                >
                    {({ errors, touched }) => (
                        <Form>
                            <DialogTitle>{editEmployee?._id ? "Edit Employee" : "Add Employee"}</DialogTitle>
                            <br />
                            <DialogContent>
                                <Grid container spacing={2} direction="column">
                                    {['email', 'solde'].map((field, index) => (
                                        <Grid item xs={12} key={index}>
                                            <Field
                                                as={TextField}
                                                fullWidth
                                                name={field}
                                                label={field.charAt(0).toUpperCase() + field.slice(1)}
                                                variant="outlined"
                                                type={field === 'solde' ? 'number' : 'text'}
                                                error={errors[field] && touched[field]}
                                                helperText={errors[field] && touched[field] ? errors[field] : ""}
                                                InputLabelProps={{ shrink: true }}
                                            />
                                        </Grid>
                                    ))}
                                </Grid>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary">Cancel</Button>
                                <Button type="submit" color="primary" variant="contained">Save</Button>
                            </DialogActions>
                        </Form>
                    )}
                </Formik>
            </Dialog>
        </Box>
    );
};

export default EmployeeManagement;