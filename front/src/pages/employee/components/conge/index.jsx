import React, { useState, useEffect } from 'react';
import { Box, Dialog, DialogTitle, DialogContent, DialogActions, Button, Grid, TextField,useTheme } from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useSelector } from 'react-redux';
import Toast from 'react-hot-toast';

import { tokens } from "../../../../theme";
import Header from "../../../../components/Header";
import CustomToolbar from '../../../../components/custom-tool-bar';
import { format, differenceInCalendarDays, parseISO } from 'date-fns';

const GererConge = () => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [employeeConges, setEmployeeConges] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);
    const [loading, setLoading] = useState(false);
    const [conge, setConge] = useState({
        startDate: new Date(),
        endDate: new Date(),
        reason: '',
    });
    const [selectionModel, setSelectionModel] = useState([]);
    const token = useSelector(state => state.auth.user.accessToken);

    useEffect(() => {
        fetchAllConges();
    }, [token]);

    const fetchAllConges = async () => {
        setLoading(true);
        try {
            const response = await axios.get('http://localhost:4000/api/conges/mon-conge', {
                headers: { Authorization: `Bearer ${token}` },
            });
            const modifiedData = response.data.map(item => ({
                ...item,
                startDate: format(parseISO(item.startDate), 'dd/MM/yyyy'), 
                endDate: format(parseISO(item.endDate), 'dd/MM/yyyy'), 
            }));
            setEmployeeConges(modifiedData);
        } catch (error) {
            console.error('Failed to fetch conges:', error);
            Toast.error('Échec du chargement des congés');
        } finally {
            setLoading(false);
        }
    };

    const handleAddConge = async () => {
        setLoading(true);
        try {
            if (conge._id) {
                await axios.put(`http://localhost:4000/api/conges/update/${conge._id}`, conge, {
                    headers: { Authorization: `Bearer ${token}` },
                });
                Toast.success('Congé modifié avec succès');
            } else {
                await axios.post('http://localhost:4000/api/conges/create', conge, {
                    headers: { Authorization: `Bearer ${token}` },
                });
                Toast.success('Congé ajouté avec succès');
            }
            setOpenDialog(false);
            fetchAllConges();
        } catch (error) {
            console.error('Failed to add/update conge:', error);
            Toast.error('Échec de la sauvegarde du congé');
        } finally {
            setLoading(false);
        }
    };

    const handleDelete = async () => {
        const selectedIDs = Array.from(new Set(selectionModel));
        try {
            await Promise.all(selectedIDs.map(id =>
                axios.delete(`http://localhost:4000/api/conges/delete/${id}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
            ));
            setEmployeeConges(employeeConges.filter(row => !selectedIDs.includes(row._id)));
        } catch (error) {
            console.error('Failed to delete conge:', error);
            Toast.error('Échec de la suppression du congé');
        }
    };

    const handleModify = () => {
        const selectedConge = employeeConges.find(conge => conge._id === selectionModel[0]);
        setConge(selectedConge);
        setOpenDialog(true);
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setConge(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const handleDateChange = (name, value) => {
        setConge(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const handleOpenDialog = () => {
        setConge({ startDate: new Date(), endDate: new Date(), reason: ''});
        setOpenDialog(true);
    };

    const handleClose = () => {
        setOpenDialog(false);
    };

    const columns = [
        { field: 'startDate', headerName: 'Date de Début', width: 150 },
        { field: 'endDate', headerName: 'Date de Fin', width: 150 },
        { field: 'status', headerName: 'Statut', width: 120 },
        { field: 'reason', headerName: 'Raison', width: 200 },
    ];

    return (
        <Box m="20px">
            <Header title="Gérer les Congés" subtitle="Liste des Congés" />
            <Box
                m="40px 0 0 0"
                height="75vh"
                sx={{
                    "& .MuiDataGrid-root": { border: "none" },
                    "& .MuiDataGrid-cell": { borderBottom: "none" },
                    "& .MuiDataGrid-columnHeaders": { backgroundColor: colors.blueAccent[700], borderBottom: "none" },
                    "& .MuiDataGrid-virtualScroller": { backgroundColor: colors.primary[400] },
                    "& .MuiDataGrid-footerContainer": { borderTop: "none", backgroundColor: colors.blueAccent[700] },
                    "& .MuiCheckbox-root": { color: `${colors.greenAccent[200]} !important` },
                }}
            >
                <DataGrid
                    checkboxSelection
                    loading={loading}
                    rows={employeeConges}
                    columns={columns}
                    components={{ Toolbar: CustomToolbar }}

                    getRowId={(row) => row._id}
                    onSelectionModelChange={(newSelectionModel) => {
                        setSelectionModel(newSelectionModel);
                    }}
                    componentsProps={{
                        toolbar: { onAdd: handleOpenDialog, onDelete: handleDelete, onModify: handleModify },
                    }}
                />
            </Box>
            <Dialog open={openDialog} onClose={handleClose}>
                <DialogTitle>{'Ajouter / Modifier un Congé'}</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <label>Date de Début</label>
                            <TextField
                                type="date"
                                value={conge.startDate}
                                onChange={(e) => handleDateChange('startDate', e.target.value)}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <label>Date de Fin</label>
                            <TextField
                                type="date"
                                value={conge.endDate}
                                onChange={(e) => handleDateChange('endDate', e.target.value)}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <label>Raison</label>
                            <TextField
                                fullWidth
                                variant="outlined"
                                name="reason"
                                value={conge.reason}
                                onChange={handleChange}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">Annuler</Button>
                    <Button onClick={() => handleAddConge(conge)} color="primary" variant="contained">Sauvegarder</Button>
                </DialogActions>
            </Dialog>
        </Box>
    );
};

export default GererConge;
