    import React, { useEffect, useState } from 'react';
    import { Box, TextField, Button, Typography, Container } from '@mui/material';
    import axios from 'axios';
    import { useSelector } from 'react-redux';

    const UserProfile = () => {
        const token = useSelector(state => state.auth.user.accessToken);
        const role = useSelector(state => state.auth.user.userInfo.role);
        const userId = useSelector(state => state.auth.user.userInfo._id);

        const [user, setUser] = useState({
            email: '',
            password: '',
            role: ''
        });

        useEffect(() => {
            fetchUser();
        }, [userId, token, role]);

        const fetchUser = async () => {
            const url = `http://localhost:4000/api/user/${userId}`;
            try {
                const response = await axios.get(url, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                setUser({ ...response.data, role }); 
            } catch (error) {
                console.error('Failed to fetch user:', error);
            }
        };

        const handleChange = (event) => {
            const { name, value } = event.target;
            setUser(prevUser => ({
                ...prevUser,
                [name]: value
            }));
        };

        const handleSave = async () => {
            const url = `http://localhost:4000/api/user/${userId}`;
            try {
                const response = await axios.put(url, user, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                console.log(response.data);
                alert('Mise à jour du profil réussie!');
            } catch (error) {
                console.error('Failed to update profile:', error);
                alert('Error updating profile!');
            }
        };

        return (
            <Container maxWidth="xs">
                <Box sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                    <Typography component="h1" variant="h5">
                        Edit Profile
                    </Typography>
                    <Box component="form" noValidate sx={{ mt: 1 }}>
                        <TextField
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            value={user.email}
                            onChange={handleChange}
                            margin="normal"
                        />
                        <TextField
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="new-password"
                            value={user.password}
                            onChange={handleChange}
                            margin="normal"
                        />
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                            onClick={handleSave}
                        >
                            Save Changes
                        </Button>
                    </Box>
                </Box>
            </Container>
        );
    };

    export default UserProfile;
