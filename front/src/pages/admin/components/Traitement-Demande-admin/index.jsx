import React, { useState, useEffect } from 'react';
import {
    Box, useTheme, Button, Dialog, DialogActions, DialogContent, DialogTitle,
    TextField, Grid
} from '@mui/material';
import { DataGrid } from '@mui/x-data-grid';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { tokens } from "../../../../theme";
import Header from "../../../../components/Header";
import Toast from 'react-hot-toast';
import { format, differenceInCalendarDays, parseISO } from 'date-fns';

const AdminTraitementDemande = () => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [leaveData, setLeaveData] = useState([]);
    const token = useSelector(state => state.auth.user.accessToken);

    useEffect(() => {
        fetchLeaves();
    }, [token]);

    const fetchLeaves = async () => {
        try {
            const response = await axios.get('http://localhost:4000/api/conges/all', {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            const modifiedData = response.data.map(item => ({
                ...item,
                employeeEmail: item.employee?.email || 'N/A',
                formattedStartDate: format(parseISO(item.startDate), 'dd/MM/yyyy'), 
                formattedEndDate: format(parseISO(item.endDate), 'dd/MM/yyyy'), 
                daysBetween: differenceInCalendarDays(parseISO(item.endDate), parseISO(item.startDate)) + 1 
            }));
            setLeaveData(modifiedData);
        } catch (error) {
            console.error('Failed to fetch leaves:', error);
        }
    };

    const handleAccept = async (id) => {
        try {
            await axios.put(`http://localhost:4000/api/conges/accepter/${id}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            Toast.success('Leave accepted successfully');
            fetchLeaves();
        } catch (error) {
            Toast.error( error.response.data.error);
            console.error('Failed to accept leave:', error.response.data.error);
        }
    };

    const handleRefuse = async (id) => {
        try {
            await axios.put(`http://localhost:4000/api/conges/refuser/${id}`, {}, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            Toast.success('Leave refused successfully');
            fetchLeaves();
        } catch (error) {
            console.error('Failed to refuse leave:', error);
        }
    };

    const columns = [
        { field: 'employeeEmail', headerName: 'Employee Email', width: 200 },
        { field: 'formattedStartDate', headerName: 'Start Date', width: 130 },
        { field: 'formattedEndDate', headerName: 'End Date', width: 130 },
        { field: 'daysBetween', headerName: 'Days', width: 90, type: 'number' },
        { 
            field: 'status', 
            headerName: 'Status', 
            width: 100,
            renderCell: (params) => {
                const getStatusStyles = (status) => {
                    switch (status) {
                        case 'Pending':
                            return { color: 'orange', fontWeight: 'bold' };
                        case 'Approved':
                            return { color: 'green', fontWeight: 'bold' };
                        case 'Rejected':
                            return { color: 'red', fontWeight: 'bold' };
                        default:
                            return { color: 'black' };
                    }
                };
                return <span style={getStatusStyles(params.value)}>{params.value}</span>;
            }
        },
        { field: 'reason', headerName: 'Reason', width: 150 },
        {
            field: 'actions', headerName: 'Actions', width: 300, sortable: false, renderCell: (params) => (
                <>
                    <Button
                        color="success"
                        onClick={() => handleAccept(params.id)}
                        disabled={params.row.status !== 'Pending'}
                    >
                        Accept
                    </Button>
                    <Button
                        color="error"
                        onClick={() => handleRefuse(params.id)}
                        disabled={params.row.status !== 'Pending'}
                    >
                        Refuse
                    </Button>
                </>
            ),
        },
    ];

    return (
        <Box m="20px">
            <Header title="Traitement Demande" subtitle="Manage Employee Leaves" />
            <Box
                m="40px 0 0 0"
                height="75vh"
                sx={{
                    "& .MuiDataGrid-root": { border: "none" },
                    "& .MuiDataGrid-cell": { borderBottom: "none" },
                    "& .MuiDataGrid-columnHeaders": { backgroundColor: colors.blueAccent[700], borderBottom: "none" },
                    "& .MuiDataGrid-virtualScroller": { backgroundColor: colors.primary[400] },
                    "& .MuiDataGrid-footerContainer": { borderTop: "none", backgroundColor: colors.blueAccent[700] },
                    "& .MuiCheckbox-root": { color: `${colors.greenAccent[200]} !important` },
                }}
            >
                <DataGrid
                    rows={leaveData}
                    columns={columns}
                    getRowId={(row) => row._id} 

                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection={false}
                    disableSelectionOnClick
                />
            </Box>
        </Box>
    );
};

export default AdminTraitementDemande;
