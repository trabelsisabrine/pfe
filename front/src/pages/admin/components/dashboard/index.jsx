import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Box, Typography, useTheme } from '@mui/material';
import { tokens } from '../../../../theme';
import BarChart from '../../../../components/BarChart';
import PieChart from '../../../../components/PieChart';
import Header from '../../../../components/Header';
import StatBox from '../../../../components/StatBox';
import axiosInstance from '../../../../axios-instance';
const Dashboard = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  
  const [conges, setConges] = useState([]);
  const [statusDistribution, setStatusDistribution] = useState({});
  const [solde, setSolde] = useState([]);
  const [rolesDistribution, setRolesDistribution] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      try {
        const congesResponse = await axiosInstance.get('/api/kpi/conges/current-month');
        setConges(congesResponse.data);

        const statusDistributionResponse = await axiosInstance.get('/api/kpi/conges/status-distribution');
        setStatusDistribution(statusDistributionResponse.data);

        const soldeResponse = await axiosInstance.get('/api/kpi/users/solde');
        setSolde(soldeResponse.data);

        const rolesDistributionResponse = await axiosInstance.get('/api/kpi/users/roles-distribution');
        setRolesDistribution(rolesDistributionResponse.data);
      } catch (error) {
        console.error("Error fetching data", error);
      }
    };
    
    fetchData();
  }, []);

  return (
    <Box m="20px">
      <Header title="tableau de bord" subtitle="Bienvenue sur votre tableau de bord" />

      <Box display="flex" justifyContent="space-between">
        <StatBox title="Nombre total d'utilisateurs" value={solde.length} />
        <StatBox title="Congés approuvés" value={statusDistribution.approved} />
        <StatBox title="Congés rejetées" value={statusDistribution.rejected} />
      </Box>

      <hr />

      <Box mt="20px" display="grid" gridTemplateColumns="repeat(12, 1fr)" gridAutoRows="140px" gap="20px">
        <Box gridColumn="span 12" gridRow="span 2" backgroundColor={colors.primary[400]}>
          <Box mt="25px" p="0 30px" display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h5" fontWeight="600" color={colors.grey[100]}>
            Conges du mois en cours
            </Typography>
          </Box>
          <Box height="250px" m="-20px 0 0 0">
            <BarChart data={conges.map(conge => ({
              country: conge.reason,
              value: Math.max(0, (new Date(conge.endDate) - new Date(conge.startDate)) / (1000 * 60 * 60 * 24)) 
            }))} />
          </Box>
        </Box>

        <Box gridColumn="span 6" gridRow="span 2" backgroundColor={colors.primary[400]}>
          <Box mt="25px" p="0 30px" display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h5" fontWeight="600" color={colors.grey[100]}>
            Répartition des statuts de congé
            </Typography>
          </Box>
          <Box height="250px" m="-20px 0 0 0">
            <PieChart data={[
              { id: 'Approved', label: 'Approved', value: statusDistribution.approved },
              { id: 'Rejected', label: 'Rejected', value: statusDistribution.rejected },
              { id: 'Pending', label: 'Pending', value: statusDistribution.pending }
            ]} />
          </Box>
        </Box>

        <Box gridColumn="span 6" gridRow="span 2" backgroundColor={colors.primary[400]}>
          <Box mt="25px" p="0 30px" display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h5" fontWeight="600" color={colors.grey[100]}>
            Répartition des rôles des utilisateurs
            </Typography>
          </Box>
          <Box height="250px" m="-20px 0 0 0">
            <PieChart data={[
              { id: 'RH', label: 'RH', value: rolesDistribution.rh },
              { id: 'Admin', label: 'Admin', value: rolesDistribution.admin },
              { id: 'Employee', label: 'Employee', value: rolesDistribution.employee }
            ]} />
          </Box>
        </Box>

        <Box gridColumn="span 12" gridRow="span 2" backgroundColor={colors.primary[400]}>
          <Box mt="25px" p="0 30px" display="flex" justifyContent="space-between" alignItems="center">
            <Typography variant="h5" fontWeight="600" color={colors.grey[100]}>
              Solde de congé
            </Typography>
          </Box>
          <Box height="250px" m="-20px 0 0 0">
            <BarChart data={solde.map(user => ({
              country: user.email,
              value: user.solde
            }))} />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Dashboard;
