import { Box, useTheme, Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button, Select, MenuItem, IconButton, InputAdornment } from '@mui/material';
import { DataGrid } from "@mui/x-data-grid";
import { useEffect, useState } from "react";
import axios from "axios";
import { tokens } from "../../../../theme";
import Header from "../../../../components/Header";
import CustomToolbar from '../../../../components/custom-tool-bar';
import { useSelector } from 'react-redux';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Toast from 'react-hot-toast';

const Users = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [userData, setUserData] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [editingUser, setEditingUser] = useState({ id: '', email: '', role: '', password: '' });
  const [showPassword, setShowPassword] = useState(false);
  const [formErrors, setFormErrors] = useState({});

  const token = useSelector((state) => state.auth.user.accessToken);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const res = await axios.get('http://localhost:4000/api/user/all', {
        headers: { Authorization: `Bearer ${token}` },
      });
      setUserData(res.data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleAdd = () => {
    setEditingUser({ _id: '', password:'', email: '', role: '' }); 
    setOpenDialog(true);
    setFormErrors({});
  };

  const handleModify = (user) => {
    setEditingUser(user); 
    setOpenDialog(true);
    setFormErrors({});
  };

  const handleDelete = async (user) => {
    try {
      await axios.delete(`http://localhost:4000/api/user/${user._id}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
      fetchUsers(); 
    } catch (err) {
      console.error("Error deleting the user:", err);
    }
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const handleSave = async () => {
    if (!editingUser.email || !editingUser.password || !/\S+@\S+\.\S+/.test(editingUser.email)) {
      setFormErrors({
        email: 'Please enter a valid email address.',
        password: 'Please enter a password.'
      });
      return;
    }

    const method = editingUser._id ? 'put' : 'post';
    const url = editingUser._id ? `http://localhost:4000/api/user/${editingUser._id}` : 'http://localhost:4000/api/auth/register';

    const { _id , ...rest } = editingUser;
    try {
      await axios[method](url, rest, {
        headers: { Authorization: `Bearer ${token}` },
      });
      Toast.success('Utilisateur enregistré avec succès');
      fetchUsers();
      handleClose();
    } catch (err) {
      console.error("Error saving the user:", err);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEditingUser(prev => ({ ...prev, [name]: value }));
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const columns = [
    { field: "_id", headerName: "ID", width: 300 },
    { field: "email", headerName: "Email", width: 200 },
    { field: "role", headerName: "Role", width: 150 },
    { field: 'actions', headerName: 'Actions', width: 300, renderCell: (params) => (
      <>
        <Button color="primary" onClick={() => handleModify(params.row)}>Edit</Button>
        <Button color="error" onClick={() => handleDelete(params.row)}>Delete</Button>
      </>
    )}
  ];

  return (
    <Box m="20px">
      <Header title="Users" subtitle="Liste des utilisateurs" />
      <Box m="40px 0 0 0" height="75vh" sx={{
        "& .MuiDataGrid-root": { border: "none" },
        "& .MuiDataGrid-cell": { borderBottom: "none" },
        "& .MuiDataGrid-columnHeaders": { backgroundColor: colors.blueAccent[700], borderBottom: "none" },
        "& .MuiDataGrid-virtualScroller": { backgroundColor: colors.primary[400] },
        "& .MuiDataGrid-footerContainer": { borderTop: "none", backgroundColor: colors.blueAccent[700] },
        "& .MuiCheckbox-root": { color: `${colors.greenAccent[200]} !important` },
        "& .MuiDataGrid-toolbarContainer .MuiButton-text": { color: `${colors.grey[100]} !important` },
      }}>
        <DataGrid
          checkboxSelection
          rows={userData}
          columns={columns}
          getRowId={(row) => row._id} 
          components={{ Toolbar: CustomToolbar }}
          componentsProps={{
            toolbar: {
              onAdd: handleAdd,
              onDelete: handleDelete,
              onModify: handleModify,
              showModifyButton: false,
              showDeleteButton: false,
            },
          }}
        />
        <Dialog open={openDialog} onClose={handleClose}>
          <DialogTitle>{editingUser?._id ? 'Edit User' : 'Add User'}</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              name="email"
              label="Email Address"
              type="email"
              fullWidth
              variant="standard"
              value={editingUser?.email}
              onChange={handleChange}
              error={!!formErrors.email}
              helperText={formErrors.email}
            />
            <TextField
              margin="dense"
              name="password"
              label="Password"
              type={showPassword ? 'text' : 'password'}
              fullWidth
              variant="standard"
              value={editingUser?.password}
              onChange={handleChange}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={togglePasswordVisibility}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              error={!!formErrors.password}
              helperText={formErrors.password}
            />
            <Select
              name="role"
              label="Role"
              value={editingUser.role}
              onChange={handleChange}
              fullWidth
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value="rh">RH</MenuItem>
              <MenuItem value="employee">Employee</MenuItem>
              <MenuItem value="admin">ADMIN</MenuItem>
            </Select>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Annuler</Button>
            <Button onClick={handleSave}>sauvegarder</Button>
          </DialogActions>
        </Dialog>
      </Box>
    </Box>
  );
};

export default Users;
