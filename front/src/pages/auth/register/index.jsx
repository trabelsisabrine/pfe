import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import { Link } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import axios from 'axios';
import Toast from "react-hot-toast";
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import Lottie from 'lottie-react';
import animation from '../../../assets/login.json'; 

const defaultTheme = createTheme();

export default function Register() {
  const navigate = useNavigate();

  

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    console.log("🚀 ~ handleSubmit ~ data:", data.get('email'), data.get("password"))
    

    try {
      const res = await axios.post('http://localhost:4000/api/auth/register', {
        email: data.get('email'),
        password: data.get('password')
      })
      if (res.data){
        Toast.success('Utilisateur créé avec succès')
        navigate('/login')
      }
    } catch (error) {
      console.log(error)
    }
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Grid container component="main" sx={{ height: '100vh', backgroundColor: 'silver' }}>
        <CssBaseline />
        <Grid item xs={12} sm={6} md={5} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Box
            sx={{
              backgroundColor: 'white',
              p: 8,
              borderRadius: 2,
              boxShadow: 3,
              width: { xs: '90%', sm: '75%', md: '60%' },
            }}
          >
            <Grid container justifyContent="center" alignItems="center">
  <Grid item>
    <Avatar sx={{ m: 1, bgcolor: 'blue' }}>
      <LockOutlinedIcon />
    </Avatar>
  </Grid>
</Grid>
            <Typography component="h1" variant="h4" align="center" gutterBottom>
              Inscription
            </Typography>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Adresse Email"
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Mot de passe"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                  />
                </Grid>
               
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                S'inscrire
              </Button>
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <span>Compte existant ? </span>
                  <Link to="/login" style={{ color: 'black' }}>
                    Se connecter
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={12} sm={6} md={7} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Lottie animationData={animation} style={{ width: '80%', height: '100%' }} />
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}