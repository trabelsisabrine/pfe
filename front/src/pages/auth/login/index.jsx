import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import axios from 'axios';
import Toast from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../../../redux/actions/authActions';
import { Link } from 'react-router-dom';
import Lottie from 'lottie-react';
import animation from '../../../assets/anim.json'; 

const isValidEmail = (email) => {
  return /\S+@\S+\.\S+/.test(email);
}

export default function Login() {
  const currentTheme = useTheme();

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [error, setError] = React.useState(null);
  const [showPassword, setShowPassword] = React.useState(false);

  const handleTogglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const email = data.get('email');

    if (!data.get('email') || !data.get('password')) {
      Toast.error('Merci de remplir tous les champs vide');
      return;
    }
    if (!isValidEmail(email)) {
      Toast.error('Please enter a valid email address');
      return;
    }

    try {
      const response = await axios.post('http://localhost:4000/api/auth/login', {
        email: data.get('email'),
        password: data.get('password')
      });
      console.log(response)
      if (response.status === 200) {
        Toast.success('Connecté avec succès');
        localStorage.setItem('login', JSON.stringify(response.data));
        dispatch(loginSuccess(response.data));
        console.log(response.data)
        switch (response.data.userInfo.role) {
          case 'admin':
            navigate('/admin/users');
            break;
          case 'employee':
            navigate('/employee');
            break;
          case 'rh':
            navigate('/rh/gerer-conge');
            break;
        }
      }
    } catch (err) {
      Toast.error(err.response.data.message);
    }
  };

  return (
    <Grid container component="main" sx={{ height: '100vh', backgroundColor: 'silver' }}>
      <CssBaseline />
      <Grid item xs={12} sm={6} md={7} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Lottie animationData={animation} style={{ width: '80%', height: '100%' }} />
      </Grid>
      <Grid item xs={12} sm={10} md={5}  elevation={100} square sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Box
          sx={{
            backgroundColor: 'white',
            p: 8,
            borderRadius: 2,
            boxShadow: 3,
            width: { xs: '90%', sm: '75%', md: '60%' },
          }}
        >
      <Grid container justifyContent="center" alignItems="center">
  <Grid item>
    <Avatar sx={{ m: 1, bgcolor: 'blue' }}>
      <LockOutlinedIcon />
    </Avatar>
  </Grid>
</Grid>
          <Typography component="h1" variant="h5" align="center" gutterBottom>
            Authentification
          </Typography>
          <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Adresse e-mail"
              name="email"
              type="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type={showPassword ? 'text' : 'password'}
              id="password"
              autoComplete="current-password"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleTogglePasswordVisibility}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              se connecter
            </Button>
            {error && (
              <Typography variant="body2" color="error" align="center">
                {error}
              </Typography>
            )}
          </Box>
          
          <Typography variant="body2" align="center" sx={{ mt: 2 }}>
            Je n'ai pas de compte?{' '}
            <Link to="/register" className="text-black-500 hover:underline">
              S'inscrire
            </Link>
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );
}
